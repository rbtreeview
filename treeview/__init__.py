# -*- Mode: python; coding: utf-8; tab-width: 8; indent-tabs-mode: t; -*- 
#
# Copyright (C) 2010 - Christophe Oosterlynck and Dries Langsweirdt
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.

import rhythmdb, rb
import gtk
import gobject

from TreeViewSource import TreeViewSource

# Adding UI (xml)
toolbar_ui = """
<ui>
  <toolbar name="ToolBar">
    <separator/>
      <toolitem name="ToggleTreeView" action="ToggleTreeView"/>
  </toolbar>
</ui>
"""

# TreeView Activiation/Deactivation
class TreeView(rb.Plugin):
    
    def __init__(self):
        rb.Plugin.__init__(self)

    def activate(self, shell):
        print "Activating TreeView Plugin"
        
        self.shell = shell 

	# Add Source
	db = shell.get_property("db")
	model = db.query_model_new_empty()
	self.treeviewsource = gobject.new (TreeViewSource, shell=shell, name=_("TreeViewSource"), query_model=model)
	shell.append_source(self.treeviewsource, None)

        # Add the Toolbar Action
        action = gtk.Action('ToggleTreeView', _('Toggle Tree View'),
                            _("Show or Hide Tree View"),
                            'gtk-add')
        action.connect('activate', self.toggle_tree_view, shell)
        self.action_group = gtk.ActionGroup('TreeViewActionGroup')
        self.action_group.add_action(action)	
        ui_manager = shell.get_ui_manager()        
        ui_manager.insert_action_group(self.action_group)
        self.ui_id = ui_manager.add_ui_from_string(toolbar_ui)

	ui_manager.ensure_update()

    def deactivate(self, shell):
       	print "Deactivating TreeView Plugin"

        ui_manager = shell.get_ui_manager() 
        ui_manager.remove_ui (self.ui_id)       
        ui_manager.remove_action_group(self.action_group)
        self.action_group = None
	self.treeviewsource.delete_thyself()
        del self.shell

    # User Action
    def toggle_tree_view(a, b, c):
        print "Doing toggle action"            



